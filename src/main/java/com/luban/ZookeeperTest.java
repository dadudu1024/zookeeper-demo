package com.luban;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;

public class ZookeeperTest {

    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        // 连接服务端，连接地址可以写多个，比如"127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183"
        // 当客户端与服务端的连接断掉后就会重试去连其他的服务器地址
        // watcher

        // 初始化timeout
        // 启动SendThread(socket, 初始化， 读写事件, 发送时), EventTrhead
        // outgoingqueue packet pendingqueue

        /**
         * connectString 连接地址可以写多个，比如"127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183",当客户端与服务端的Socket连接断掉后就会重试去连其他的服务器地址
         * sessionTimeout 客户端设置的话会超时时间
         * watcher 监听器
         */
//        ZooKeeper zooKeeper = new ZooKeeper("192.168.40.52:2181", 60 * 1000, new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println(watchedEvent);
//            }
//        });


        ZooKeeper zooKeeper = new ZooKeeper("192.168.50.52:2181", 60 * 1000, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                System.out.println(watchedEvent);
            }
        });

        // 创建一个节点，并设置内容，设置ACL(该节点的权限设置)，
        // 节点类型（7种：持久节点、临时节点、持久顺序节点、临时顺序节点、容器节点、TTL节点、TTL顺序节点）
//        String result = zooKeeper.create("/luban123", "123".getBytes(),
//                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
//        System.out.println(result);

//        Stat stat = new Stat();
//        byte[] data = zooKeeper.getData("/luban", false, stat);
//        System.out.println(new String(data));



        // 节点是否存在
//        Stat stat = zooKeeper.exists("/luban123", false);
//        System.out.println(stat);  // 如果节点不存在，则stat为null


        // 修改节点的内容，这里有乐观锁,version表示本次修改, -1表示不检查版本强制更新
        // stat表示修改数据成功之后节点的状态
//        Stat stat = zooKeeper.setData("/luban", "xxx".getBytes(), -1);

        // 删除
//        zooKeeper.delete("/luban123", -1);
//        System.in.read();


        // 创建子节点
//        String result = zooKeeper.create("/luban/luban123", "123".getBytes(),
//                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
//        System.out.println(result);

        // 获取子节点
//        Stat stat = new Stat();
//        List<String> children = zooKeeper.getChildren("/luban", false, stat);
//        System.out.println(children);

        // 给/luban节点绑定了一个监听，用来监听节点的数据变化和当前节点的删除事件
//        Stat stat = new Stat();
//        zooKeeper.getData("/luban", new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println(watchedEvent);
//            }
//
//        }, stat);

        // 给/luban节点绑定了一个监听，用来监听节点的子节点的变化，子节点的增加和删除
//        zooKeeper.getChildren("/luban", new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println("child");
//            }
//        });

        // 给/luban123节点绑定了一个监听，用来监听节点的创建事件
//        zooKeeper.exists("/luban123", new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println(watchedEvent);
//            }
//        });


        // 给/luban123节点绑定了一个持久化监听，可以监听节点的创建、删除、修改、增加子节点、删除子节点事件
//        zooKeeper.addWatch("/luban123", new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println(watchedEvent);
//            }
//        }, AddWatchMode.PERSISTENT);


        // 给/luban123节点绑定了一个持久化监听，可以监听节点的创建、删除、修改、增加子节点、删除子节点事件
        // 并且还会监听/luban123的所有子节点的变化，包括子节点的创建、删除、修改
//        zooKeeper.addWatch("/luban123", new Watcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) {
//                System.out.println(watchedEvent);
//            }
//        }, AddWatchMode.PERSISTENT_RECURSIVE);


//        zooKeeper.create("/luban123/xxx_", "123".getBytes(),
//                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT_SEQUENTIAL, new AsyncCallback.StringCallback() {
//                    @Override
//                    public void processResult(int i, String s, Object o, String s1) {
//                        System.out.println(i); // 命令执行结果，可以参考KeeperException.Code枚举
//                        System.out.println(s); // 传入的path
//                        System.out.println(o); // 外部传进来的ctx
//                        System.out.println(s1);// 创建成功后的path，比如顺序节点
//
//                    }
//                }, "ctx");


//        zooKeeper.create("/luban123/xxx_", "123".getBytes(),
//                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT_SEQUENTIAL, new AsyncCallback.Create2Callback() {
//                    @Override
//                    public void processResult(int i, String s, Object o, String s1, Stat stat) {
//                        System.out.println(i);      // 命令执行结果，可以参考KeeperException.Code枚举
//                        System.out.println(s);      // 传入的path
//                        System.out.println(o);      // 外部传进来的ctx
//                        System.out.println(s1);     // 创建成功后的path，比如顺序节点
//                        System.out.println(stat);   // 创建出来的节点的stat
//                    }
//                }, "ctx");

//        System.in.read();


    }
}
