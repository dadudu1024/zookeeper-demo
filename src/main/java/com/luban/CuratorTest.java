package com.luban;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

public class CuratorTest {

    public static void main(String[] args) throws Exception {


//        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
//        CuratorFramework client = CuratorFrameworkFactory.newClient("192.168.40.52:2181", retryPolicy);
//        client.start();

        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString("192.168.50.52:2181")
                .sessionTimeoutMs(60 * 1000)  // 会话超时时间
                .connectionTimeoutMs(5000) // 连接超时时间
                .retryPolicy(retryPolicy)
                .build();
        client.start();

        // 创建节点
//        String path = client.create().forPath("/luban-curator", "123".getBytes());
//        System.out.println(path);

//        byte[] bytes = client.getData().forPath("/luban-curator");
//        System.out.println(new String(bytes));

//        Stat stat = client.checkExists().forPath("/luban-curator");
//        System.out.println(stat);

//        Stat stat = client.setData().forPath("/luban-curator", "456".getBytes());
//        System.out.println(stat);

//        client.delete().forPath("/luban-curator");

//        String path = client.create().forPath("/luban-curator/test", "xxx".getBytes());
//        System.out.println(path);

//        List<String> list = client.getChildren().forPath("/luban-curator");
//        System.out.println(list);

//        client.createContainers("/luban-curator-containers");

//        String path = client.create().withTtl(3000).withMode(CreateMode.PERSISTENT_WITH_TTL).forPath("/luban-ttl");
//        System.out.println(path);


//        client.getData().usingWatcher(new CuratorWatcher() {
//            @Override
//            public void process(WatchedEvent watchedEvent) throws Exception {
//                System.out.println(watchedEvent);
//            }
//        }).forPath("/luban");


        // NodeCache监听自身节点的数据变化
//        NodeCache nodeCache = new NodeCache(client, "/luban");
//        nodeCache.start();
//        nodeCache.getListenable().addListener(new NodeCacheListener() {
//            @Override
//            public void nodeChanged() throws Exception {
//                System.out.println(123);
//            }
//        });


        // PathChildrenCache能够监听自身节点下的子节点的变化
//        PathChildrenCache pathChildrenCache = new PathChildrenCache(client, "/luban", true);
//        pathChildrenCache.start();
//
//        pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {
//            @Override
//            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent event) throws Exception {
//                switch (event.getType()) {
//                    case CHILD_ADDED: {
//                        System.out.println("Node added: " + ZKPaths.getNodeFromPath(event.getData().getPath()));
//                        break;
//                    }
//
//                    case CHILD_UPDATED: {
//                        System.out.println("Node changed: " + ZKPaths.getNodeFromPath(event.getData().getPath()));
//                        break;
//                    }
//
//                    case CHILD_REMOVED: {
//                        System.out.println("Node removed: " + ZKPaths.getNodeFromPath(event.getData().getPath()));
//                        break;
//                    }
//                }
//            }
//        });


        // TreeCache即能够监听自身节点的变化，也能监听子节点的变化
//        TreeCache treeCache = new TreeCache(client, "/luban");
//        treeCache.start();
//        treeCache.getListenable().addListener(new TreeCacheListener() {
//            @Override
//            public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent event) throws Exception {
//                if (event.getData() != null) {
//                    System.out.println("type=" + event.getType() + " path=" + event.getData().getPath());
//                } else {
//                    System.out.println("type=" + event.getType());
//                }
//            }
//        });

//        System.in.read();


        // 一个监听器
        // 节点创建，子节点的创建
        // 节点内容的改变，子节点内容的改变
        // 节点的删除，子节点的删除
//        CuratorCacheListener listener = CuratorCacheListener.builder()
//                .forCreates(node -> System.out.println(String.format("Node created: [%s]", node)))
//                .forChanges((oldNode, node) -> System.out.println(String.format("Node changed. Old: [%s] New: [%s]", oldNode, node)))
//                .forDeletes(oldNode -> System.out.println(String.format("Node deleted. Old value: [%s]", oldNode)))
//                .forInitialized(new Runnable() {
//                    @Override
//                    public void run() {
//                        System.out.println("Cache initialized");
//                    }
//                })
//                .build();
//
//
//        CuratorCache curatorCache = CuratorCache.build(client, "/luban");
//        curatorCache.listenable().addListener(CuratorCacheListener.builder().forAll(listener).build());
//        curatorCache.start();
//
//        System.in.read();


//        String s = client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).inBackground(new BackgroundCallback() {
//            @Override
//            public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
//                System.out.println(curatorEvent);
//            }
//        }).forPath("/luban/h_", "123".getBytes());
//
//        System.out.println(s);
//
//        System.in.read();

    }
}
